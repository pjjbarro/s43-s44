let app = new Vue({
  el: '#app',
  data: {
    storage: [],
    length: 0,
    category: '',
    staff: '',
    course: ''

  },
  methods: {
    addCourse: function() {
      if (!(this.course && this.staff && this.category)) {
        return
      }
      this.storage.push({
        course: this.course,
        staff: this.staff,
        category: this.category,
      });
      this.length++;
      this.course = '';
      this.staff = '';
      this.category = '';
    },
    removeCourse: function(data) {
      this.storage = this.storage.filter((value) => data != value);
      this.length--;
    }

  }
});
